class Api::VoluntariosController < ApplicationController
    respond_to :json
  
    def index
      respond_with Voluntario.all
    end
  
    def show
      respond_with Voluntario.find(params[:id])
    end
  
    def create
      respond_with :api, Voluntario.create(voluntario_params)
    end
  
    def destroy
      respond_with Voluntario.destroy(params[:id])
    end
  
    def update
      voluntario = Voluntario.find(params['id'])
      voluntario.update(voluntario_params)
      respond_with Voluntario, json: voluntario
    end
  
    private
  
    def voluntario_params
      params.require(:voluntario).permit(
        :id,
        :matricula,
        :nombre,
        :carrera,
        :semestre,
        :graduando,
        :genero,
        :motivo,
        :experiencia,
        :talla,
        :status
      )
    end
  end