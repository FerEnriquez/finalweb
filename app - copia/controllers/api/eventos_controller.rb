class Api::EventosController < ApplicationController
    respond_to :json
  
    def index
      respond_with Evento.order(fecha_ini: :DESC)
    end
  
    def show
      respond_with Evento.find(params[:id])
    end
  
    def create
      respond_with :api, Evento.create(evento_params)
    end
  
    def destroy
      respond_with Evento.destroy(params[:id])
    end
  
    def update
      evento = Evento.find(params['id'])
      evento.update(evento_params)
      respond_with Evento, json: evento
    end
  
    private
  
    def evento_params
      params.require(:evento).permit(
        :id,
        :nombre,
        :fecha_ini,
        :fecha_fin,
        :horas,
        :ingreso,
        :deuda,
        :status
      )
    end
  end