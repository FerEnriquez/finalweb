class Api::ActividadesController < ApplicationController
    respond_to :json
  
    def index
      respond_with Actividad.all    
    end
  
    def show
      respond_with Actividad.find(params[:id])
    end
  
    def create
      respond_with :api, Actividad.create(actividad_params)
    end
  
    def destroy
      respond_with Actividad.destroy(params[:id])
    end
  
    def update
      actividad = Actividad.find(params['id'])
      actividad.update(actividad_params)
      respond_with Actividad, json: actividad
    end
  
    private
  
    def actividad_params
      params.require(:actividad).permit(
        :id,
        :matricula,
        :nombreV,
        :nombreE,
        :horas,
        :ingreso,
        :deuda,
        :completado
      )
    end
  end