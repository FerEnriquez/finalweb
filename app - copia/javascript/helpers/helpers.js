export const isEmptyObject = obj => Object.keys(obj).length === 0;

export const validateVoluntario = (voluntario) => {
  const errors = {};

  if (voluntario.matricula === '') {
    errors.matricula = 'You must enter an event type';
  }

  if (voluntario.nombre === '') {
    errors.nombre = 'You must enter a valid date';
  }

  if (voluntario.carrera === '') {
    errors.carrera = 'You must enter a title';
  }

  if (voluntario.semestre === '') {
    errors.semestre = 'You must enter at least one speaker';
  }

  if (voluntario.motivo === '') {
    errors.motivo = 'You must enter at least one host';
  }

  if (voluntario.experiencia === '') {
    errors.experiencia = 'You must enter at least one host';
  }

  return errors;
}

export const formatDate = (d) => {
  const YYYY = d.getFullYear();
  const MM = `0${d.getMonth() + 1}`.slice(-2);
  const DD = `0${d.getDate()}`.slice(-2);

  return `${YYYY}-${MM}-${DD}`;
};