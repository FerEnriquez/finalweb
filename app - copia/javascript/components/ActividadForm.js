import React from 'react';
import PropTypes from 'prop-types';
import { isEmptyObject } from '../helpers/helpers';  

class ActividadForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      actividad: props.actividad,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.dateInput = React.createRef();
  }

  handleSubmit(e) {
    e.preventDefault();
    const { actividad } = this.state;
    const errors = this.validateActividad(actividad);
    
    if (!this.isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(actividad);
    }
  }

  validateActividad(actividad) {
    const errors = {};

    if (actividad.nombre === '') {
      errors.nombre = 'Debes ingresar el nombre de tu evento.';
    }
    if (actividad.matricula === '') {
      errors.nombre = 'Debes ingresar la matricula de tu voluntario.';
    }

    if (actividad.horas === '') {
      errors.horas = 'Indica las horas que vale tu evento';
    }
    if (isNaN(actividad.horas)) {
      errors.horas = 'Las horas deben ser números';
    }
  
  }

  isEmptyObject(obj) {
    return Object.keys(obj).length === 0;
  }

  componentWillReceiveProps({ actividad }) {
    this.setState({ actividad });
  }

  updateActividad(key, value) {
    this.setState(prevState => ({
      actividad: {
        ...prevState.actividad,
        [key]: value,
      },
    }));
  }

  handleInputChange(actividad) {
    const { target } = actividad;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updateActividad(name, value);

    this.setState(prevState => ({
      actividad: {
        ...prevState.actividad,
        [name]: value,
      },
    }));
  }


  renderErrors() {
    const { errors } = this.state;

    if (this.isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the actividad from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { actividad } = this.state;

    return (
      <div className="conactBody">
        {this.renderErrors()}
        <form className="form" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="matricula">
              <input
                type="text"
                id="matricula"
                name="matricula"
                className="name entry " 
                placeholder="Matricula voluntario"
                onChange={this.handleInputChange}
                value={actividad.matricula}
              />
            </label>
          </div>
          <div>
            <label htmlFor="evento">
              <input
                type="text"
                id="nombreE"
                name="nombreE"
                className="name entry " 
                placeholder="Nombre del evento"
                onChange={this.handleInputChange}
                value={actividad.nombreE}
              />
            </label>
          </div>
          <div>
            <label htmlFor="horas">
              <input
                type="text"
                id="horas"
                name="horas"
                className="name entry " 
                placeholder="Horas de servicio"
                onChange={this.handleInputChange}
                value={actividad.horas}
              />
            </label>
          </div>
          <div>
            <label htmlFor="ingreso">
              <input
                type="text"
                id="ingreso"
                name="ingreso"
                className="name entry " 
                placeholder="Monto ingreso"
                onChange={this.handleInputChange}
                value={actividad.ingreso}
              />
            </label>
          </div>
          <div>
            <label htmlFor="deuda">
              <input
                type="text"
                id="deuda"
                name="deuda"
                className="name entry " 
                placeholder="Monto faltante"
                onChange={this.handleInputChange}
                value={actividad.deuda}
              />
            </label>
          </div>
          <div>
            <label htmlFor="completado" class="container">Activo
              <input
                type="checkbox"
                id="completado"
                name="completado"
                onChange={this.handleInputChange}
                checked={actividad.completado}
              />
              <span class="checkmark"></span>
            </label>
            
          </div>
          <div className="form-actions">
            <button className="submit entry" type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

ActividadForm.propTypes = {
    actividad: PropTypes.shape(),
    onSubmit: PropTypes.func.isRequired,
};

ActividadForm.defaultProps = {
  actividad: {
        nombreV: '',
        matricula: '',
        nombreE: '',
        horas: '',
        ingreso: '',
        deuda: '',
        completado: false,
  },
};

export default ActividadForm;