import React from 'react';
import axios from 'axios';
import Header from './Header';
import EventoList from './EventoList';
import PropTypes from 'prop-types';
import PropsRoute from './PropsRoute';
import Evento from './Evento';
import { Switch } from 'react-router-dom';
import EventoForm from './EventoForm';

class EditorEvento extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      eventos: null,
    };
    this.addEvento = this.addEvento.bind(this);
    this.deleteEvento = this.deleteEvento.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/eventos.json')
      .then(response => this.setState({ eventos: response.data }))
      .catch((error) => {
        console.log(error);
      });
  }

  addEvento(newEvento) {
    axios
      .post('/api/eventos.json', newEvento)
      .then((response) => {
        alert('Evento guardado!');
        const savedEvento = response.data;
        this.setState(prevState => ({
          eventos: [...prevState.eventos, savedEvento],
        }));
        const { history } = this.props;
        history.push(`/eventos/${savedEvento.id}`);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  
  deleteEvento(eventoId) {
    const sure = window.confirm('Are you sure?');
    if (sure) {
      axios
        .delete(`/api/eventos/${eventoId}.json`)
        .then((response) => {
          if (response.status === 204) {
            alert('Evento eliminado');
            const { history } = this.props;
            history.push('/eventos');

            const { eventos } = this.state;
            this.setState({ eventos: eventos.filter(evento => evento.id !== eventoId) });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  render() {
    const { eventos } = this.state;
    if (eventos === null) return null;

    const { match } = this.props;
    const eventoId = match.params.id;
    const evento = eventos.find(v => v.id === Number(eventoId));

    return (
        <div>
            <Header/>
              <div className="grid">
                <EventoList eventos={eventos} activeId={Number(eventoId)} />
                <Switch>
                <PropsRoute path="/eventos/new" component={EventoForm} onSubmit={this.addEvento} />
                <PropsRoute path="/eventos/:id" component={Evento} evento={evento} onDelete={this.deleteEvento}/>
                <PropsRoute path="/eventos/:id" component={Evento} Evento={evento} />
                </Switch>
              </div>
        </div>
    );
  }
}

EditorEvento.propTypes = {
    match: PropTypes.shape(),
    history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};
  
EditorEvento.defaultProps = {
    match: undefined,
};  

export default EditorEvento;