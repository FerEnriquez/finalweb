import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class VoluntarioList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        searchTerm: '',
    };

    this.searchInput = React.createRef();
    this.updateSearchTerm = this.updateSearchTerm.bind(this);
  }

  updateSearchTerm() {
    this.setState({ searchTerm: this.searchInput.current.value });
  }

  matchSearchTerm(obj) {
    const {
       ...rest
    } = obj;
    const { searchTerm } = this.state;
  
    return Object.values(rest).some(
      value => value.toString().toLowerCase().indexOf(searchTerm.toString().toLowerCase()) > -1,
    );
  }

  renderVoluntarios() {
    const { activeId, voluntarios } = this.props;
    const filteredVoluntarios = voluntarios
      .filter(el => this.matchSearchTerm(el));

    return filteredVoluntarios.map(voluntario => (
      <li key={voluntario.id}>
        <Link to={`/voluntarios/${voluntario.id}`}className={activeId === voluntario.id ? 'active' : ''}>
            {voluntario.matricula}
            {' | '}
            {voluntario.nombre}
         </Link>
      </li>
    ));
  }



  render() {
    return (
      <section className="eventList">
        <h2>Voluntarios</h2>
        <input className="search"
          placeholder="Buscar"
          type="text"
          ref={this.searchInput}
          onKeyUp={this.updateSearchTerm}
        />
        <Link to="/voluntarios/new" className="new">Nuevo voluntario</Link>
        <ul>{this.renderVoluntarios()}</ul>
      </section>
    );
  }
}

VoluntarioList.propTypes = {
  activeId: PropTypes.number,
  voluntarios: PropTypes.arrayOf(PropTypes.object),
};

VoluntarioList.defaultProps = {
  activeId: undefined,
  voluntarios: [],
};

export default VoluntarioList;