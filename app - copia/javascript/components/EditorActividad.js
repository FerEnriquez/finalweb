import React from 'react';
import axios from 'axios';
import Header from './Header';
import PropTypes from 'prop-types';
import PropsRoute from './PropsRoute';
import ActividadList from './ActividadList';
import ActividadForm from './ActividadForm';
import Actividad from './Actividad';
import { Switch } from 'react-router-dom';

class EditorActividad extends React.Component {
  constructor(props) {
    super(props);

    this.updateActvidad = this.updateActividad.bind(this);

    this.state = {
      actividades: null,
    };
  }

  updateActividad(updatedActividad) {
    axios
      .put(`/api/actividades/${updatedActvidad.id}.json`, updatedaActividad)
      .then(() => {
        success('Actividad completada');
        const { actividades } = this.state;
        const idx = actividades.findIndex(actividad => actividad.id === updatedActividad.id);
        actividades[idx] = updatedActividad;
        const { history } = this.props;
        history.push(`/actividades/${updatedActvidad.id}`);
        this.setState({ actividades });
      })
      .catch(handleAjaxError);
  }

  componentDidMount() {
    axios
      .get('/api/actividades.json')
      .then(response => this.setState({ actividades: response.data }))
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const { actividades } = this.state;
    if (actividades === null) return null;

    const { match } = this.props;
    const actividadId = match.params.id;
    const actividad = actividades.find(v => v.id === Number(actividadId));

    return (
        <div>
            <Header/>
              <div className="grid">
                <ActividadList actividades={actividades} activeId={Number(actividadId)}/>
                <Switch>
                <PropsRoute path="/actividades/:id" component={Actividad} actividad={actividad}/>
                <PropsRoute path="/actividades/:id/edit" component={ActividadForm} actividad={actividad} onSubmit={this.updateActividad}/>
                </Switch>
              </div>
        </div>
    );
  }
}

EditorActividad.propTypes = {
    match: PropTypes.shape(),
};
  
EditorActividad.defaultProps = {
    match: undefined,
};  

export default EditorActividad;