import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
  <header>
    <h1>  </h1>
    <img className="logo" src="http://localhost:3000/images/logo.png" height="150px" width="150px"></img>
    <h1> | Supercompucampu Campus Puebla |</h1>
    <Link className="backMenu" to="/menu">Menu</Link>
  </header>
);

export default Header;