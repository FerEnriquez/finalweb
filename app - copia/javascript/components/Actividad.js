import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Actividad = ({ actividad }) => (
  <div className="actividadCont">
    <h1 className="edit">
      {'  |  '}
      {actividad.matricula}
      {'  |  '}
      {actividad.nombreV}
      {'  |  '}
      <Link className="edit"  to={`/actividades/${actividad.id}/edit`}>Editar</Link>
    </h1>
    <h3>{'Horas totales: '} 
    {actividad.completado ? actividad.horas : '0'}</h3>
    
    <div className="tbl-header">
    <table cellPadding="0" cellSpacing="0" border="0">
      <thead>
        <tr>
          <th><strong>Evento</strong></th>
          <th><strong>Ingreso</strong></th>
          <th><strong>Deuda</strong></th>
          <th><strong>Horas</strong></th>
          <th><strong>Completado</strong></th>
        </tr>
      </thead>
      </table>
    </div>
    <div>
      <table className="tbl-content"> 
      <tbody>
        <tr>
          <td>{actividad.nombreE}</td>
          <td>{actividad.ingreso}</td>
          <td>{actividad.deuda}</td>
          <td>{actividad.horas}</td>
          <td>{actividad.completado ? 'Si' : 'No'}</td>
        </tr>
        </tbody>
      </table>
      </div>
  </div>
);

Actividad.propTypes = {
    actividad: PropTypes.shape(),
};

Actividad.defaultProps = {
    actividad: undefined,
};

export default Actividad;