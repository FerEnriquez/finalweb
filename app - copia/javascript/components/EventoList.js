import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class EventoList extends React.Component {
  renderEventos() {
    const { activeId, eventos } = this.props;
    return eventos.map(evento => (
      <li key={evento.id}>
        <Link to={`/eventos/${evento.id}`}className={activeId === evento.id ? 'active' : ''}>
            {evento.nombre}
         </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="eventList">
        <h2>Eventos</h2>
        <Link to="/eventos/new" className="new">Nuevo evento</Link>
        <ul>{this.renderEventos()}</ul>
      </section>
    );
  }
}

EventoList.propTypes = {
  activeId: PropTypes.number,
  eventos: PropTypes.arrayOf(PropTypes.object),
};

EventoList.defaultProps = {
  activeId: undefined,
  eventos: [],
};

export default EventoList;