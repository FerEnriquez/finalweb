import React from 'react';
import PropTypes from 'prop-types';
import Pikaday from 'pikaday';
import 'pikaday/css/pikaday.css';
import { formatDate, isEmptyObject, validateEvent } from '../helpers/helpers';  

class EventoForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      evento: props.evento,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.dateInput = React.createRef();
  }

  handleSubmit(e) {
    e.preventDefault();
    const { evento } = this.state;
    const errors = this.validateEvento(evento);
    
    if (!this.isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(evento);
    }
  }

  validateEvento(evento) {
    const errors = {};

    if (evento.nombre === '') {
      errors.nombre = 'Debes ingresar un nombre para tu evento.';
    }

    if (evento.horas === '') {
      errors.horas = 'Indica las horas que vale tu evento';
    }
    if (isNaN(evento.horas)) {
      errors.horas = 'Las horas deben ser números';
    }
  

    console.log(evento);
    return errors;
  }

  isEmptyObject(obj) {
    return Object.keys(obj).length === 0;
  }

  updateEvent(key, value) {
    this.setState(prevState => ({
      evento: {
        ...prevState.evento,
        [key]: value,
      },
    }));
  }

  handleInputChange(evento) {
    const { target } = evento;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updateEvent(name, value);

    this.setState(prevState => ({
      evento: {
        ...prevState.evento,
        [name]: value,
      },
    }));
  }

  componentDidMount() {
    new Pikaday({
      field: this.dateInput.current,
      onSelect: (date) => {
        const formattedDate = formatDate(date);
        this.dateInput.current.value = formattedDate;
        this.updateEvent('event_date', formattedDate);
      },
    });
  }

  renderErrors() {
    const { errors } = this.state;

    if (this.isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the Evento from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    return (
      <div className="conactBody">
        {this.renderErrors()}
        <form className="form" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="nombre">
              <input
                type="text"
                id="nombre"
                name="nombre"
                className="name entry " 
                placeholder="Nombre"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="fecha_ini">
              <input
                type="text"
                id="fecha_ini"
                name="fecha_ini"
                className="name entry " 
                placeholder="Fecha"
                ref={this.dateInput}
                autoComplete="off"
              />
            </label>
          </div>
          <div>
            <label htmlFor="horas">
              <input
                type="text"
                id="horas"
                name="horas"
                className="name entry " 
                placeholder="Horas de servicio"
                onChange={this.handleInputChange}
              />
            </label>
          </div>

          <div>
            <label htmlFor="status" class="container">Activo
              <input
                type="checkbox"
                id="status"
                name="status"
                onChange={this.handleInputChange}
              />
              <span class="checkmark"></span>
            </label>
            
          </div>
          <div className="form-actions">
            <button className="submit entry" type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

EventoForm.propTypes = {
    evento: PropTypes.shape(),
    onSubmit: PropTypes.func.isRequired,
};

EventoForm.defaultProps = {
  evento: {
        nombre: '',
        fecha_ini: '',
        fecha_fin: '',
        horas: '',
        ingreso: '',
        deuda: '',
        status: false,
  },
};

export default EventoForm;