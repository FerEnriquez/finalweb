import React from 'react';

const VoluntarioNotFound = () => <p>Voluntario no encontrado!</p>;

export default VoluntarioNotFound;