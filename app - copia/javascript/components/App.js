import React from 'react';
import { Route } from 'react-router-dom';
import EditorVoluntario from './EditorVoluntario';
import EditorEvento from './EditorEvento';
import EditorActividad from './EditorActividad';
import EditorMenu from './EditorMenu';
import './App.css';

const App = () => (
  <div>
    <Route path="/voluntarios/:id?" component={EditorVoluntario} />
    <Route path="/eventos/:id?" component={EditorEvento} />
    <Route path="/actividades/:id?" component={EditorActividad} />
    <Route path="/menu" component={EditorMenu} />
  </div>
);

export default App;