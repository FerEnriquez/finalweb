import React from 'react';
import PropTypes from 'prop-types';

const Evento = ({ evento, onDelete }) => (
  <div className="eventContainer">
  <div>
    <h2>
      {evento.nombre}
      <button className="delete" type="button" onClick={() => onDelete(evento.id)}> Borrar </button>
    </h2>
    <ul>
      <li>
        <strong>Horas de servicio:</strong>
        {' '}
        {evento.horas}
      </li>
      <li>
        <strong>Fecha inicio:</strong>
        {' '}
        {evento.fecha_ini}
      </li>
      <li>
        <strong>Fecha termino:</strong>
        {' '}
        {evento.fecha_fin}
      </li>
      <li>
        <strong>Activo:</strong>
        {' '}
        {evento.status ? 'Si' : 'No'}
      </li>
    </ul>
  </div>
  </div>
);

Evento.propTypes = {
  evento: PropTypes.shape(),
    onDelete: PropTypes.func.isRequired,
};

Evento.defaultProps = {
  evento: undefined,
};

export default Evento;