import React from 'react';

class EditorMenu extends React.Component {
  goToVoluntarios = () => {
    window.location = "/voluntarios";
  } 
  goToActividades = () => {
    window.location = "/actividades";
  } 
  goToEventos = () => {
    window.location = "/eventos";
  } 
  render() {
    return (
        <div className="menuContent">
            <img  src="http://localhost:3000/images/logo.png" height="500px" width="500px"></img>
            <button className="menu" onClick={this.goToVoluntarios}>Voluntarios</button>
            <button className="menu" onClick={this.goToActividades}>Historial actividades</button>
            <button className="menu" onClick={this.goToEventos}>Eventos</button>
        </div>
    );
  }
}

EditorMenu.propTypes = {
};
  
EditorMenu.defaultProps = {
};  

export default EditorMenu;