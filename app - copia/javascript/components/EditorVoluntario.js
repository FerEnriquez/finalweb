import React from 'react';
import axios from 'axios';
import Header from './Header';
import VoluntarioList from './VoluntarioList';
import PropTypes from 'prop-types';
import PropsRoute from './PropsRoute';
import Voluntario from './Voluntario';
import { Switch } from 'react-router-dom';
import VoluntarioForm from './VoluntarioForm';

class EditorVoluntario extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      voluntarios: null,
    };
    this.addVoluntario = this.addVoluntario.bind(this);
    this.deleteVoluntario = this.deleteVoluntario.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/voluntarios.json')
      .then(response => this.setState({ voluntarios: response.data }))
      .catch((error) => {
        console.log(error);
      });
  }

  addVoluntario(newVoluntario) {
    axios
      .post('/api/voluntarios.json', newVoluntario)
      .then((response) => {
        alert('Voluntario guardado!');
        const savedVoluntario = response.data;
        this.setState(prevState => ({
          voluntarios: [...prevState.voluntarios, savedVoluntario],
        }));
        const { history } = this.props;
        history.push(`/voluntarios/${savedVoluntario.id}`);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  
  deleteVoluntario(voluntarioId) {
    const sure = window.confirm('Are you sure?');
    if (sure) {
      axios
        .delete(`/api/voluntarios/${voluntarioId}.json`)
        .then((response) => {
          if (response.status === 204) {
            alert('Voluntario eliminado');
            const { history } = this.props;
            history.push('/voluntarios');

            const { voluntarios } = this.state;
            this.setState({ voluntarios: voluntarios.filter(voluntario => voluntario.id !== voluntarioId) });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  render() {
    const { voluntarios } = this.state;
    if (voluntarios === null) return null;

    const { match } = this.props;
    const voluntarioId = match.params.id;
    const voluntario = voluntarios.find(v => v.id === Number(voluntarioId));

    return (
        <div>
            <Header/>
              <div className="grid">
                <VoluntarioList voluntarios={voluntarios} activeId={Number(voluntarioId)} />
                <Switch>
                <PropsRoute path="/voluntarios/new" component={VoluntarioForm} onSubmit={this.addVoluntario} />
                <PropsRoute path="/voluntarios/:id" component={Voluntario} voluntario={voluntario} onDelete={this.deleteVoluntario}/>
                <PropsRoute path="/voluntarios/:id" component={Voluntario} Voluntario={voluntario} />
                </Switch>
              </div>
        </div>
    );
  }
}

EditorVoluntario.propTypes = {
    match: PropTypes.shape(),
    history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};
  
EditorVoluntario.defaultProps = {
    match: undefined,
};  

export default EditorVoluntario;