import React from 'react';
import PropTypes from 'prop-types';
import { isEmptyObject, validateEvent } from '../helpers/helpers';
import VoluntarioNotFound from './VoluntarioNotFound';

class VoluntarioForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      voluntario: props.voluntario,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.dateInput = React.createRef();
  }

  handleSubmit(e) {
    e.preventDefault();
    const { voluntario } = this.state;
    const errors = this.validateVoluntario(voluntario);
    
    if (!this.isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(voluntario);
    }
  }

  validateVoluntario(voluntario) {
    const errors = {};

    if (voluntario.matricula.charAt(0) !== 'A' || voluntario.matricula === '' ) {
      errors.matricula = 'Debes ingresar una matrícula válida.';
    }

    if (voluntario.nombre === '') {
      errors.nombre = 'Debes ingresar un nombre';
    }

    if (voluntario.carrera === '' || voluntario.carrera.length > 3) {
      errors.carrera = 'Debes ingresar una carrera';
    }

    if (voluntario.semestre === '') {
      errors.semestre = 'Debes indicar el semestre';
    }
    if (isNaN(voluntario.semestre)){
      errors.semestre = 'El semestre debe ser un número';
    }

    if (voluntario.motivo === '') {
      errors.motivo = 'Debes escribir un motivo';
    }

    if (voluntario.experiencia === '') {
        errors.experiencia = 'Debes escribir alguna experiencia';
      }
  

    console.log(voluntario);
    return errors;
  }

  isEmptyObject(obj) {
    return Object.keys(obj).length === 0;
  }

  handleInputChange(voluntario) {
    const { target } = voluntario;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState(prevState => ({
        voluntario: {
        ...prevState.voluntario,
        [name]: value,
      },
    }));
  }

  renderErrors() {
    const { errors } = this.state;

    if (this.isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the voluntario from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { voluntario } = this.state;
    const { path } = this.props;
    
    if (!voluntario.id && path === '/voluntarios/:id/') return <VoluntarioNotFound />;

    const cancelURL = voluntario.id ? `/voluntarios/${voluntario.id}` : '/voluntarios';
    const title = voluntario.id ? `${voluntario.matricula} | ${voluntario.nombre}` : 'Nuevo voluntario';
    return (
      <div className="conactBody">
        {this.renderErrors()}
        <form className="form" onSubmit={this.handleSubmit}>
          <h4>Crear un nuevo evento</h4>
          <div>
            <label htmlFor="matricula">
              <input type="text" id="matricula" 
                name="matricula"
                className="name entry " 
                placeholder="Matricula"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="nombre">
              <input
                type="text"
                id="nombre"
                name="nombre"
                className="name entry " 
                placeholder="Nombre"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="clavess">
              <input
                type="text"
                id="clavess"
                name="clavess"
                className="name entry " 
                placeholder="Clave SS"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="carrera">
              <input type="text" id="carrera" name="carrera"
              className="name entry " 
              placeholder="Carrera"
              onChange={this.handleInputChange} />
            </label>
          </div>
          <div>
            <label htmlFor="semestre">
              <input type="text" id="semestre" name="semestre"
              className="name entry " 
              placeholder="Semestre"
              onChange={this.handleInputChange} />
            </label>
          </div>
          <div>
            <label htmlFor="status" className="containerG">Graduando                
              <input
                type="checkbox"
                id="graduando"
                name="graduando"
                onChange={this.handleInputChange}
              />
              <span className="checkmarkG"></span>
            </label>
          </div>
          <div>
            <label htmlFor="motivo">
              <textarea
                cols="30"
                rows="5"
                id="movtivo"
                name="motivo"
                className="message entry " 
              placeholder="Motivos"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="experiencia">
              <textarea
                cols="20"
                rows="10"
                id="experiencia"
                name="experiencia"
                className="message entry " 
                placeholder="Experiencia"
                onChange={this.handleInputChange}
              />
            </label>
          </div>
          <div>
            <label htmlFor="status" class="container">Activo                
              <input
                type="checkbox"
                id="status"
                name="status"
                onChange={this.handleInputChange}
              />
              <span class="checkmark"></span>
            </label>
          </div>
          <div className="form-actions">
            <button className="submit entry" type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

VoluntarioForm.propTypes = {
    voluntario: PropTypes.shape(),
    onSubmit: PropTypes.func.isRequired,
    path: PropTypes.string.isRequired,
};

VoluntarioForm.defaultProps = {
    voluntario: {
        matricula: '',
        nombre: '',
        carrera: '',
        semestre: '',
        motivo: '',
        experiencia: '',
        status: false,
  },
};

export default VoluntarioForm;