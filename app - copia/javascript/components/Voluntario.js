import React from 'react';
import PropTypes from 'prop-types';
import VoluntarioNotFound from './VoluntarioNotFound';

const Voluntario = ({ voluntario, onDelete }) => {
  if (!voluntario) 
    return <VoluntarioNotFound/>;

  return(
    <div className="eventContainer">
      <h2>
        {voluntario.matricula}
        {' | '}
        {voluntario.nombre}
        <button className="delete" type="button" onClick={() => onDelete(voluntario.id)}> Borrar </button>
      </h2>
      <ul>
        <li>
          <strong>Clave SS:</strong>
          {' '}
          {voluntario.clavess}
        </li>
        <li>
          <strong>Carrera:</strong>
          {' '}
          {voluntario.carrera}
        </li>
        <li>
          <strong>Semestre:</strong>
          {' '}
          {voluntario.semestre}
        </li>
        <li>
          <strong>Graduando:</strong>
          {' '}
          {voluntario.graduando ? 'Si' : 'No'}
        </li>
        <li>
          <strong>Genero:</strong>
          {' '}
          {voluntario.genero ? 'Mujer' : 'Hombre'}
        </li>
        <li>
          <strong>Motivos:</strong>
          {' '}
          {voluntario.motivo}
        </li>
          <li>
            <strong>Experiencia:</strong>
          {' '}
          {voluntario.experiencia}
        </li>
        <li>
          <strong>Talla:</strong>
          {' '}
          {voluntario.tall}
        </li>
        <li>
          <strong>Activo:</strong>
          {' '}
          {voluntario.status ? 'Si' : 'No'}
        </li>
      </ul>
    </div>
  ); 
}; 

Voluntario.propTypes = {
    voluntario: PropTypes.shape(),
    onDelete: PropTypes.func.isRequired,
};

Voluntario.defaultProps = {
    voluntario: undefined,
};

export default Voluntario;