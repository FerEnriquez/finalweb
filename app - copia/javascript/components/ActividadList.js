import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class ActividadList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        searchTerm: '',
    };

    this.searchInput = React.createRef();
    this.updateSearchTerm = this.updateSearchTerm.bind(this);
  }

  updateSearchTerm() {
    this.setState({ searchTerm: this.searchInput.current.value });
  }

  matchSearchTerm(obj) {
    const {
      ...rest
    } = obj;
    const { searchTerm } = this.state;
    return Object.values(rest).some(
      value => value.toString().toLowerCase().indexOf(
        searchTerm.toString().toLowerCase()) > -1,
    );
  }

  renderActividades() {
    const { activeId, actividades } = this.props;
    const filteredActividades = actividades
      .filter(el => this.matchSearchTerm(el));

    return filteredActividades.map(actividad => (
      <li key={actividad.id}>
        <Link to={`/actividades/${actividad.id}`}className={activeId === actividad.id ? 'active' : ''}>
            {actividad.matricula}
         </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="eventList">
        <h2>Voluntarios </h2>
        <input
          className="search"
          placeholder="Buscar"
          type="text"
          ref={this.searchInput}
          onKeyUp={this.updateSearchTerm}
        />
        <ul>{this.renderActividades()}</ul>
      </section>
    );
  }
}

ActividadList.propTypes = {
  activeId: PropTypes.number,
  actividades: PropTypes.arrayOf(PropTypes.object),
};

ActividadList.defaultProps = {
  activeId: undefined,
  actividades: [],
};

export default ActividadList;