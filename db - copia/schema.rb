# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_08_151042) do

  create_table "actividads", force: :cascade do |t|
    t.string "matricula"
    t.string "nombreV"
    t.string "nombreE"
    t.string "horas"
    t.string "ingreso"
    t.string "deuda"
    t.boolean "completado"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "eventos", force: :cascade do |t|
    t.string "nombre"
    t.date "fecha_ini"
    t.date "fecha_fin"
    t.string "horas"
    t.boolean "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "voluntarios", force: :cascade do |t|
    t.string "matricula"
    t.string "nombre"
    t.string "clavess"
    t.string "carrera"
    t.string "semestre"
    t.boolean "graduando"
    t.boolean "genero"
    t.string "talla"
    t.text "motivo"
    t.text "experiencia"
    t.boolean "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
