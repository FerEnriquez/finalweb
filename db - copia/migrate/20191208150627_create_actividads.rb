class CreateActividads < ActiveRecord::Migration[6.0]
  def change
    create_table :actividads do |t|
      t.string :matricula
      t.string :nombreV
      t.string :nombreE
      t.string :horas
      t.string :ingreso
      t.string :deuda
      t.boolean :completado

      t.timestamps
    end
  end
end
