class CreateVoluntarios < ActiveRecord::Migration[6.0]
  def change
    create_table :voluntarios do |t|
      t.string :matricula
      t.string :nombre
      t.string :clavess
      t.string :carrera
      t.string :semestre
      t.boolean :graduando
      t.boolean :genero
      t.string :talla
      t.text :motivo
      t.text :experiencia
      t.boolean :status

      t.timestamps
    end
  end
end
