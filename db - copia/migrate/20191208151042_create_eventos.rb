class CreateEventos < ActiveRecord::Migration[6.0]
  def change
    create_table :eventos do |t|
      t.string :nombre
      t.date :fecha_ini
      t.date :fecha_fin
      t.string :horas
      t.boolean :status

      t.timestamps
    end
  end
end
