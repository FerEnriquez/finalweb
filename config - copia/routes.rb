Rails.application.routes.draw do
  
  # root to: '/menu'
  get 'menu', to: 'site#index'

  # root to: '/eventos'
  get 'eventos', to: 'site#index'
  get 'eventos/new', to: 'site#index'
  get 'eventos/:id', to: 'site#index'
  get 'eventos/:id/edit', to: 'site#index'

  # root to: '/actividades'
  get 'actividades', to: 'site#index'
  get 'actividades/new', to: 'site#index'
  get 'actividades/:id', to: 'site#index'
  get 'actividades/:id/edit', to: 'site#index'

  # namespace :api do
  #   resources :eventos, only: %i[index show create destroy update]
  # end

 # root to: '/voluntarios'
  get 'voluntarios', to: 'site#index'
  get 'voluntarios/new', to: 'site#index'
  get 'voluntarios/:id', to: 'site#index'
  get 'voluntarios/:id/edit', to: 'site#index'

  namespace :api do
    resources :menu,:actividades, :voluntarios, :eventos, only: %i[index show create destroy update]
  end
end